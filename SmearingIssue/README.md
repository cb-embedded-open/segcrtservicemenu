# Smearing to the right Issue

## Description

The issue is a smearing to the right of bright elements of the image.

![Issue illustration](res/issue_illustration.png)

![Right smearing on 240p Test Suite Interface](res/240p_Test_suite_smearing.jpg)

![RGBPI Interface](res/RGBPI_Interface.jpg)

![Right smearing on RGBPI Interface](rgbpi_interface_smearing.jpg)

## Test Setup

Raspberry PI 4 with RGBPI OS connected with a RGBPI cable to the SCART input (Péritel) of the SEG CRT TV.

# Related forum posts and videos

* https://www.youtube.com/watch?v=ebOmWYqlOIY (Panasonic TV Fixed by replacing capacitors near the flyback transformer)

* https://www.reddit.com/r/crtgaming/comments/url89n/ghostingsmearing_to_the_right_is_this_my_crt/
* https://www.reddit.com/r/crtgaming/comments/d0hnmx/seeking_help_with_a_streaking_problem_on_my_pvm/
* https://shmups.system11.org/viewtopic.php?t=64782
* https://www.arcade-projects.com/threads/not-sure-what-this-issue-is-horizontal-ghosting-smearing-need-help-diagnosing.15263/ (Arcade Hantarex Polo 25 fixed by replacing resistor on the neck board)

# Diagnostic in progress

## What it is NOT

* Raspberry PI 4 and RGBPI cable
  * I tested the same setup with a different CRT TV and the issue is not present.
* Bad TV settings
    * I tried to change the settings of the TV, but the issue is still present.
    * I need to lower the brightness to make the issue less visible, but it is still present.
    * Screenshots uses the best settings I could find to diminish the issue.

## Possible Fault in the bias video bias voltage


### Checking Capacitors

Summary:

| Capacitor | Nominal Value | Measured Value | Measured ESR | Voltage Rating | Comment           |
| --------- | ------------- | -------------- | ------------ | -------------- | ----------------- |
| C613      | 10uF          | 10uF           | 1.87R        | 250V           | In-Circuit Tested |
| C827      | 33uF          |                |              | 160V           | OK (ESR Meter)    |

### Capacitor C613

The first idea was that the bias capacitors were faulty with may be an ESR too high or a capacitance too low.
I bough replacement capacitors *RUBYCON 450BXC10MEFC10X20, measured 8.82µF, 3.6R ESR*, but I also tested the original one in-circuit with my peak ESR meter. The **original capacitor** seems to be **perfect**, better than the new one I bought with a lower ESR and a higher capacitance, so I kept the original one in place.

*Note*: The in-circuit test should be fine, the capacitor is not in parallel with any other component, there is a blocking diode on one side of the node (from the EHT) and the RGB amplifier transistors on the other side.

![Measure of the Main bias capacitor with Peak ESR meter](C619_In_circuit_tested.jpg)

![C613 capacitor](<res/C613 Capacitor.jpg>)

I also tested other big capacitors on the board, they all seems to be fine.

### Capacitor C827

The capacitor C827 is another capacitor 

![C827, off circuit tested](res/C827_off_circuit_tested.jpg)

![alt text](res/C827_schematic.jpg)