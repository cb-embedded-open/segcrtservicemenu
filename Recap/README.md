# Capacitor list

List of capacitors:

| Name       | Capacitance | Usage                       | Voltage Rating | Target Voltage |
| :--------- | :---------- | :-------------------------- | :------------- | -------------- |
| C613       | 10µF        | Video B+                    | 250V           | 200V           |
| C605       | 100µF       | Flyback Gen                 | 35V            |                |
| C617       | 470µF       | 26V Source                  | 35V            | +26V           |
| C824, C825 | 100µF       | +5V Source LM317T           | 16V            | +5V            |
| C813       | 330pF       | SMPS Switch 2SK2750         | 1kV            |                |
| C832       | 100pF       | SMPS Switch 2SK2750         | 1kV            |                |
| C808       | 47µF        | Input Capacitor (BR Output) | 400V           | 230V           |
|            |             |                             |                |                |