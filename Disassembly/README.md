# Tips

Unscrew the bottom screws first. The screw is located in a fragile plastic rod that breaks if all the weight is applied to it.

# Views

|                      Left                       |                      Front                      |                      Right                      |
| :---------------------------------------------: | :---------------------------------------------: | :---------------------------------------------: |
| ![Left](img/photos/IMG_20231209_115540.jpg) | ![Front](img/photos/IMG_20231209_115546.jpg) | ![Right](img/photos/IMG_20231209_115605.jpg) |

# Infos


| Component       | Value                                                    | Photo                                            |
| --------------- | -------------------------------------------------------- | ------------------------------------------------ |
| Chassis         | [11AK36](../datasheets/11ak36vestel.pdf)                 | ![Chassis](Back_cover_11AK36_1455-56.jpg)        |
| Tube            | A34JLL80X23, DAEWOO ORION, FRANCE                        | ![Tube](CRT_A34JLL80X23.jpg)                     |
| Video chip      | [STV2248C (H22VB0223 MALTA)](../datasheets/STV223XD.PDF) | ![Video Chip](Video_STV2248C.jpg)                |
| MCU             | [ST92195C3B1 (VESTEL/T3)](../datasheets/ST92195B.PDF)    | ![MCU](MCU_ST92195C3B1.jpg)                      |
| Firmware        | ELIF/B-2 1906/B 1356                                     | ![Firmware Label](Firmwares_label.jpg)           |
| EHT             | TLF158-05 0101 2523E* C                                  | ![EHT TLF158-05](EHT_TLF158-05_0101_2523E_C.jpg) |
| Tuner           | EWT-5V3K2-E01W                                           | ![EWT-5V3K2-E01W](Tuner_EWT-5V3K2-E01W.jpg)      |
| Deflection Yoke | [ODY-M1423 E157951](../datasheets/ODY_M1423.pdf)         | ![Yoke ODY M1423](Yoke_ODY_M1423.jpg)            |
