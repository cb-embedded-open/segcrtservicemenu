---
title: "SEG CT-1402A"
author: Cédric BERENGER
output: pdf_document
---

# SEG CT-1402A (F) CRT Service Menu

![My Childhood's CRT](img/CRT_photo.jpg)

**My Good old childhood's CRT :)**

**Cool thing** : it has 4 profiles for vertical scale (50Hz 4:3, 50Hz 16:9, 60Hz 4:3, 60Hz 16:9) and 2 profiles for vertical and horizontal positions (50Hz / 60Hz).

## Investigations

My old beloved CRT was bought at Lidl stores, back in the early 2000's.
The *SEG* german brand seems to be a rebranding of TV's from the turkish brand
*Vestel*.

After hours of search, I finaly found this [Service Manual](datasheets/11ak36vestel.pdf)
for a **11ak36 Vestel Chassis**. I do not have disassembled my TV to check the 
PCBs, but I am pretty sure it is this specific board (or a very similar one).

## Enter the service menu

On remote, **☰ menu + 4725**:

* In order to enter service menu, first enter the main menu and then press the digits **4, 7, 2 and 5** respectively.
* To select adjust parameters, use **↑ or ↓** buttons. To change the selected parameter, use **← or →** buttons.
* Selected parameter will be highlighted.
* Entire service menu parameters of AK36 CHASSIS are listed below. For some of parameters the default values are given on the same table.

## Color buttons on screen menu

* 🔴 **RED BUTTON** (For Stereo models only): It switches the **AVL to ON or OFF** mode on service menu. AVL word is visible on service menu when AVL is on.
* 🟢 **GREEN BUTTON** : It switched the PICTURE MODE to **4:3 or 16:9** on service menu. It is usefull when it is necessary to adjust 16:9 picture mode vertical size.
* 🟡 **YELLOW BUTTON** : It switches to **VERTICAL SCAN DISABLE** mode. It is usefull to adjust screen voltage.
* 🔵 **BLUE BUTTON** : It is used to adjust **AGC and IF** automatically on service menu.

## Titanium E030 : default values

![Default Values (Photo)](img/DefaultValues.jpg)

| Register | Value    | Meaning                           |        Pictogram        |
| -------- | -------- | --------------------------------- | :---------------------: |
| OSD      | 065      | Horizontal Position of OSD        |                         |
| IF1      | 003      |                                   |                         |
| IF2      | 063      |                                   |                         |
| IF3      | 002      |                                   |                         |
| IF4      | 069      |                                   |                         |
| AGC      | 047      |                                   |                         |
| VLIN     | 045      | Vertical Linearity                |  ![Vlin](img/vlin.jpg)  |
| VS1A     | 030      | Vertical Size PAL **50Hz 4:3**    | ![Vsize](img/vsize.jpg) |
| VS1B     | 052      | Vertical Size PAL **50Hz 16:9**   | ![Vsize](img/vsize.jpg) |
| VP1      | 004      | Vertical Position PAL **50Hz**    |  ![Vpos](img/vpos.jpg)  |
| HP1      | 029      | Horizontal Position PAL **50Hz**  |  ![Hpos](img/hpos.jpg)  |
| VS2A     | 002      | Vertical Size NTSC **60Hz 4:3**   | ![Vsize](img/vsize.jpg) |
| VS2B     | 045      | Vertical Size NTSC **60Hz 16:9**  | ![Vsize](img/vsize.jpg) |
| VP2      | 008      | Vertical Position NTSC **60Hz**   |  ![Vpos](img/vpos.jpg)  |
| HP2      | 025      | Horizontal Position NTSC **60Hz** |  ![Hpos](img/hpos.jpg)  |
| RGBH     | 007      | RGB Horizontal Offset             |                         |
| WR       | 040      | White Balance Adjustment R        |                         |
| WG       | 040      | White Balance Adjustment G        |                         |
| WB       | 040      | White Balance Adjustment B        |                         |
| BR       | 031      |                                   |                         |
| BG       | 031      |                                   |                         |
| APR      | 010      |                                   |                         |
| FMP1     | 009      |                                   |                         |
| NIP1     | 020      |                                   |                         |
| SCP1     | 014      |                                   |                         |
| FMP2     | 018      |                                   |                         |
| NIP2     | 039      |                                   |                         |
| SCP2     | 014      |                                   |                         |
| F1H      | 00001100 |                                   |                         |
| F1L      | 01001010 |                                   |                         |
| F2H      | 00011110 |                                   |                         |
| F2L      | 00100010 |                                   |                         |
| BS1      | 10100001 |                                   |                         |
| BS2      | 10010010 |                                   |                         |
| BS3      | 00110100 |                                   |                         |
| CB       | 10001110 |                                   |                         |
| OP1      | 01101101 |                                   |                         |
| OP2      | 01011000 |                                   |                         |
| OP3      | 10110101 | Video Options                     |                         |
| OP4      | 01111111 |                                   |                         |
| OP5      | 11111011 |                                   |                         |
| TX1      | 00010111 | Teletext Options                  |                         |
|          |          |                                   |                         |

## Options enabled by default

OP3: 1 Xtal PAL/SEC/NTSC 4.43
Blue Background when no signal AV.
White insertion On

## 60Hz (NTSC) Profile Adjustment

![Before / After](img/60HzAdjust.jpg)

Simply adjust: 

* **VS2A** (Vertical Size in 4:3 60Hz mode)
* **VP2** (Vertical Position in 4:3 / 16:9 60Hz modes)
* **HP2** (Horizontal Position in 4:3 / 16:9 60Hz modes)

To get a perfect picture.

## Terms Glossary

| Term | Meaning                      |
| ---- | ---------------------------- |
| AVL  | Automatic Volume Leveling    |
| OSD  | On Screen Display (TV Menus) |
| AGC  | Automatic Gain Control       |

## Service Settings : full description

Extracted from [11ak36 Vestel Chassis Service Manual (CRT Driver Board)](img/11ak36vestel.pdf)

| REGISTER | PARAMETER                                      | NOTE (NUMBERS ARE DEFAULT VALUES FOR CONCERNED PARAMETER) |
| :------- | :--------------------------------------------- | :-------------------------------------------------------- |
| OSD      | OSD Horizontal Position                        | ADJUST HORIZONTAL POSITION FOR OSD                        |
| IF1      | IF Coarse Adjust                               | IF1 Adjust Course Neg. Adj. (WO / L)                     |
| IF2      | IF Fine Adjust                                 | IF2 Adjust Fine Neg. Adj. (WO / L)                       |
| IF3      | IF Coarse Adjust for L-Prime                   | IF3 Adjust Course Pos. Adj. (W / L)                      |
| IF4      | IF Fine Adjust for L-Prime                     | IF4 Adjust Fine Pos. Adj. (W / L)                        |
| AGC      | Automatic Gain Control                         | AGC Adjust AGC                                            |
| VLIN     | Vertical Linearity                             | ADJUST VERTICAL LINEARITY                                 |
| VS1A     | Vertical Size for 50 Hz / 4:3                  | ADJUST VERTICAL SIZE FOR 4:3 MODE (50 HZ)                 |
| VS1B     | Vertical Size for 50 Hz / 16:9                 | ADJUST VERTICAL SIZE FOR 16:9 MODE (50 HZ)                |
| VP1      | Vertical Position for 50 Hz                    | ADJUST VERTICAL POSITION (50 HZ)                          |
| HP1      | Horizontal Position for 50 Hz                  | ADJUST HORIZONTAL POSITION (50 HZ)                        |
| VS2A     | Vertical Size for 60 Hz / 4:3                  | ADJUST VERTICAL SIZE FOR 4:3 MODE (60 HZ)                 |
| VS2B     | Vertical Size for 60 Hz / 16:9                 | ADJUST VERTICAL SIZE FOR 16:9 MODE (60 HZ)                |
| VP2      | Vertical Position for 60 Hz                    | ADJUST VERTICAL POSITION (60 HZ)                          |
| HP2      | Horizontal Position for 60 Hz                  | ADJUST HORIZONTAL POSITION (60 HZ)                        |
| RGBH     | RGB Horizontal Shift Offset                    | CVBS  RGB HORIZONTAL POSITION COMPENSATION               |
| WR       | White Point Adjust for RED                     | 40                                                        |
| WG       | White Point Adjust for GREEN                   | 40                                                        |
| WB       | White Point Adjust for BLUE                    | 40                                                        |
| BR       | Bias for RED                                   | 31                                                        |
| BG       | Bias for GREEN                                 | 31                                                        |
| APR      | APR Threshold                                  | 10                                                        |
| FMP1     | FM Prescaler when AVL is OFF                   | 9 (STEREO ONLY)                                           |
| NIP1     | NICAM Prescaler when AVL is OFF                | 20 (STEREO ONLY)                                          |
| SCP1     | SCART Prescaler when AVL is OFF                | 13 (STEREO ONLY)                                          |
| FMP2     | FM Prescaler when AVL is ON                    | 13 (STEREO ONLY)                                          |
| NIP2     | NICAM Prescaler when AVL is ON                 | 16 (STEREO ONLY)                                          |
| SCP2     | SCART Prescaler when AVL is ON                 | 13 (STEREO ONLY)                                          |
| F1H      | High Byte of crossover frequency for VHF1-VHF3 | MEANINGFUL FOR ONLY PLL TUNER (see tuner setting table)   |
| F1L      | Low Byte of crossover frequency for VHF1-VHF3  | MEANINGFUL FOR ONLY PLL TUNER (see tuner setting table)   |
| F2H      | High Byte of crossover frequency for VHF3-UHF  | MEANINGFUL FOR ONLY PLL TUNER (see tuner setting table)   |
| F2L      | Low Byte of crossover frequency for VHF3-UHF   | MEANINGFUL FOR ONLY PLL TUNER (see tuner setting table)   |
| BS1      | Band Switch Byte for VHF1 Meaningful for only  | MEANINGFUL FOR ONLY PLL TUNER (see tuner setting table)   |
| BS2      | Band Switch Byte for VHF3 Meaningful for only  | MEANINGFUL FOR ONLY PLL TUNER (see tuner setting table)   |
| BS3      | Band Switch Byte for UHF Meaningful for only   | MEANINGFUL FOR ONLY PLL TUNER (see tuner setting table)   |
| CB       | Control Byte Meaningful for only PLL Tuner     | MEANINGFUL FOR ONLY PLL TUNER (see tuner setting table)   |
| OP1      | Option 1 (see the Option List)                 | PERIPHERAL OPTIONS (see option table)                     |
| OP2      | Option 2 (see the Option List)                 | RECEPTION STANDART OPTIONS (see option table)             |
| OP3      | Option 3 (see the Option List)                 | VIDEO OPTIONS (see option table)                          |
| OP4      | Option 4 (see the Option List)                 | TV FEATURE OPTIONS (see option table)                     |
| OP5      | Option 5 (see the Option List)                 | CHANNEL TABLE OPTIONS (see option table)                  |
| TX1      | Teletext Option 1 (see the Option List)        | TELETEXT OPTIONS (see option table)                       |

## Remote control codes

Uses RC5 IR protocol with address 0:

![RC5](img/RC5.png)

(Src: http://www.datelec.fr/fiches/RC5.html)

[SEG IR File (Flipper Zero)](./Seg.ir)

![Remote Control](img/RemoteControl.jpg)

## 240p Test suite for PS2

https://www.youtube.com/watch?v=4Ho_F1-7hxA

[ISO of 240p Test Suite (via Genesis Emulation)](img/240p Test Suite.iso)
