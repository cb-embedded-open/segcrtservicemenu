---
title: "1AK36 Chassis"
author: Cédric BERENGER
output: pdf_document
---


# Schematic 11AK36 Chassis

## Intro

Generic TV repair instructions, troubleshooting guide :

* https://www.repairfaq.org/sam/tvfaq.htm

Vertical Deflection : 

![Vertical Deflection](Vertical-Deflection-Circuit-in-TV-1.jpg)

* https://www.eeeguide.com/vertical-deflection-circuit-in-tv/



## Main components

* [MCU ST ST92195B (OSD and config)](../datasheets/ST92195B.PDF)
* [Video Controller STV223XD (Video PAL / NTSC / RGB and OSD Mix)](../datasheets/STV223XD.PDF)
* [TDA8174AW Vertical Deflection Circuit](../datasheets/TDA8174A.PDF)

## General Circuit

![General Circuit](general.png)

### Top of the board

(Photo taken from the internet: [el service](https://el-service.org/2sc5300.html/shassi-vestel-transistor-2sc5388))

![11AK36](vestel_11ak36_el_service.jpg)

### Bottom of the board

(Photo taken from the internet, mirrored and super-imposed : [El forum](https://www.elforum.info/topic/32159-eurocolor-11ak36/) )

![11AK36 Back](vest_11ak36_bottom_superimpose_100.jpg)

## CRT RGB Amplifiers

On auxiliary board (directly connected to the CRT)

![CRT Schematic](CRT.jpg)

### Single Linear Amplifier

![Linear Amplifier](Linear.png)

## EHT Deflection

![EHT Schematic](EHT.jpg)

## MCU

![MCU Schematic](MCU.jpg)

## Video IC

![Video IC Schematic](Video.jpg)

## SMPS

![SMPS Schematic](SMPS.jpg)

## Connectors F-AV / SCART

![Connectors F-AV / SCART Schematic](FAV_SCART.jpg)

## Location of components

### Main Board

![Location of components](Board_11AK36_Overlay.jpg)

### Aux Board

![CRT board (IA)](11ak6_CRT_Board_IA.jpeg)

![CRT Board](11ak36_CRT_Board_Populated.jpg)